module "repo-config_frontend" {
  source  = "gitlab.com/vigigloo/tf-modules-gitlab/projectcommonconfiguration"
  version = "0.0.1"

  project_id  = data.gitlab_project.frontend.id
  helm_values = file("${path.module}/config/frontend.yml")
}

