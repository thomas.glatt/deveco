locals {
  environment-slug = var.gitlab_environment_scope == "*" ? "review" : var.gitlab_environment_scope
}

module "kubeconfig_thomas" {
  source  = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version = "0.1.0"

  filename               = "deveco-thomas-${local.environment-slug}.yml"
  namespace              = module.namespace.namespace
  username               = "thomas"
  project_slug           = var.project_slug
  project_name           = var.project_name
  role_name              = module.namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}
module "kubeconfig_augustin" {
  source  = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version = "0.1.0"

  filename               = "deveco-augustin-${local.environment-slug}.yml"
  namespace              = module.namespace.namespace
  username               = "augustin"
  project_slug           = var.project_slug
  project_name           = var.project_name
  role_name              = module.namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}
