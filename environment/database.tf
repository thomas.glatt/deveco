locals {
  db_replicas = contains(["development", "production"], var.gitlab_environment_scope) ? 3 : 1
  db_backups  = contains(["development", "production"], var.gitlab_environment_scope)
}
module "postgresql" {
  source  = "gitlab.com/vigigloo/tools-k8s-crunchydata/pgcluster"
  version = "0.0.13"

  chart_name = "postgresql"
  namespace  = module.namespace.namespace
  values = [
    file("${path.module}/database.yaml"),
    local.db_backups ?
    templatefile("${path.module}/backups.yaml", {
      bucket_name         = var.backup_bucket_name
      scaleway_access_key = var.scaleway_access_key
      scaleway_secret_key = var.scaleway_secret_key
      backup_offset_min   = "0"
    }) : ""
  ]
  pg_replicas = local.db_replicas
}

resource "gitlab_project_variable" "db-host" {
  project           = var.backend_project_id
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_ENV_VAR_POSTGRES_HOST"
  value = module.postgresql.host
}

resource "gitlab_project_variable" "db-database" {
  project           = var.backend_project_id
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_ENV_VAR_POSTGRES_DB"
  value = module.postgresql.dbname
}

resource "gitlab_project_variable" "db-user" {
  project           = var.backend_project_id
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_ENV_VAR_POSTGRES_USERNAME"
  value = module.postgresql.user
}

resource "gitlab_project_variable" "db-password" {
  project           = var.backend_project_id
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_ENV_VAR_POSTGRES_PASSWORD"
  value = module.postgresql.password
}
