provider "scaleway" {
  alias      = "scaleway_development"
  region     = "fr-par"
  zone       = "fr-par-1"
  access_key = var.scaleway_cluster_development_access_key
  secret_key = var.scaleway_cluster_development_secret_key
  project_id = var.scaleway_cluster_development_project_id
}

provider "scaleway" {
  alias      = "scaleway_production"
  region     = "fr-par"
  zone       = "fr-par-1"
  access_key = var.scaleway_cluster_production_access_key
  secret_key = var.scaleway_cluster_production_secret_key
  project_id = var.scaleway_cluster_production_project_id
}
