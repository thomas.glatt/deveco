data "scaleway_k8s_cluster" "prod" {
  provider   = scaleway.scaleway_production
  cluster_id = var.scaleway_cluster_production_cluster_id
}

data "scaleway_k8s_cluster" "dev" {
  provider   = scaleway.scaleway_development
  cluster_id = var.scaleway_cluster_development_cluster_id
}

data "gitlab_project" "frontend" {
  id = 34122769
}

data "gitlab_project" "backend" {
  id = 34153013
}

data "gitlab_group" "deveco" {
  group_id = 16575463
}
