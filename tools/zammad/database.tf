module "postgresql" {
  source  = "gitlab.com/vigigloo/tools-k8s-crunchydata/pgcluster"
  version = "0.0.13"

  chart_name = "postgresql"
  namespace  = module.namespace.namespace
  values = [
    templatefile("${path.module}/backups.yaml", {
      bucket_name         = var.backup_bucket_name
      scaleway_access_key = var.scaleway_access_key
      scaleway_secret_key = var.scaleway_secret_key
      backup_offset_min   = var.backup_offset_minutes
    })
  ]

  pg_volume_size = "10Gi"
  pg_replicas    = 3
}
