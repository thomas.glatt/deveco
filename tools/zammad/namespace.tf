module "namespace" {
  source       = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version      = "0.1.0"
  max_cpu      = 12
  max_memory   = "18Gi"
  namespace    = "${var.project_slug}-zammad"
  project_name = "Zammad"
  project_slug = "zammad"
}
