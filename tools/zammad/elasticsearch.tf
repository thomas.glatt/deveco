module "elasticsearch" {
  source           = "gitlab.com/vigigloo/tools-k8s/elasticsearch"
  version          = "0.2.0"
  chart_name       = "elastic"
  namespace        = module.namespace.namespace
  image_repository = "registry.gitlab.com/vigigloo/tools/elasticsearch"
  image_tag        = "7.17.1-ingest-attachment"
  values = [
    file("${path.module}/elasticsearch.yaml"),
  ]

  elasticsearch_replicas         = 2
  elasticsearch_persistence_size = "20Gi"
  requests_cpu                   = "500m"
  requests_memory                = "1Gi"
  limits_cpu                     = "1000m"
  limits_memory                  = "2Gi"
}
