resource "kubernetes_cron_job" "zammad_snapshot" {
  metadata {
    name      = "zammad-snapshot"
    namespace = module.namespace.namespace
  }

  spec {
    concurrency_policy            = "Replace"
    failed_jobs_history_limit     = 10
    schedule                      = "50 3 * * SAT"
    starting_deadline_seconds     = 10
    successful_jobs_history_limit = 10

    job_template {
      metadata {
        annotations = {}
        labels      = {}
      }

      spec {
        backoff_limit = 3

        template {
          metadata {
            annotations = {}
            labels      = {}
          }
          spec {
            container {
              name  = "scaleway-snapshot"
              image = "registry.gitlab.com/vigigloo/tools/scaleway-pvc-snapshot:0.2.1-scw-2.5.1"

              resources {
                limits = {
                  cpu    = "500m"
                  memory = "512Mi"
                }
              }

              env {
                name  = "SCW_ACCESS_KEY"
                value = var.scaleway_access_key
              }
              env {
                name  = "SCW_SECRET_KEY"
                value = var.scaleway_secret_key
              }
              env {
                name  = "SCW_DEFAULT_ORGANIZATION_ID"
                value = var.scaleway_organization_id
              }
              env {
                name  = "SCW_DEFAULT_PROJECT_ID"
                value = var.scaleway_project_id
              }
              env {
                name  = "SCW_DEFAULT_ZONE"
                value = "fr-par-1"
              }
              env {
                name  = "VOLUME_HANDLE"
                value = "fr-par-1/2d36f4e9-a8ba-4356-bc41-0b0e6169a100"
              }
              env {
                name  = "SNAPSHOT_NAME"
                value = "deveco-zammad"
              }
            }
          }
        }
      }
    }
  }
}
