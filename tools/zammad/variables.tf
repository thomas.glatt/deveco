variable "hostname" {
  type = string
}

variable "project_slug" {
  type = string
}

variable "scaleway_organization_id" {
  type = string
}
variable "scaleway_project_id" {
  type = string
}
variable "scaleway_access_key" {
  type = string
}
variable "scaleway_secret_key" {
  type      = string
  sensitive = true
}
variable "backup_bucket_name" {
  type = string
}
variable "backup_offset_minutes" {
  type        = number
  description = "Minute at which the backup should take place. Used to prevent all backups from running at the same time."
}
