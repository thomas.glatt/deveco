module "zammad" {
  source     = "gitlab.com/vigigloo/tools-k8s/zammad"
  version    = "0.1.1"
  chart_name = "zammad"
  namespace  = module.namespace.namespace
  values = [
    templatefile("${path.module}/zammad.yaml", {
      hostname = "${var.hostname}"
    }),
  ]

  zammad_persistence_size = "30Gi"
  redis_host              = module.redis.hostname
  redis_password          = resource.random_password.redis_password.result
  memcached_host          = module.memcached.hostname
  elasticsearch_host      = module.elasticsearch.hostname
  postgresql_host         = module.postgresql.host
  postgresql_user         = module.postgresql.user
  postgresql_password     = replace(module.postgresql.password, ",", "\\,")
  postgresql_database     = module.postgresql.dbname
}
