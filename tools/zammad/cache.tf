resource "random_password" "redis_password" {
  length  = 32
  special = false
}

module "redis" {
  source         = "gitlab.com/vigigloo/tools-k8s/redis"
  version        = "0.1.1"
  chart_name     = "redis"
  namespace      = module.namespace.namespace
  redis_password = resource.random_password.redis_password.result
  requests_cpu   = "500m"
  limits_cpu     = "1000m"
  redis_replicas = 0
}

module "memcached" {
  source     = "gitlab.com/vigigloo/tools-k8s/memcached"
  version    = "0.1.1"
  chart_name = "memcached"
  namespace  = module.namespace.namespace
}
