module "gitlab-runner" {
  source  = "gitlab.com/vigigloo/tf-modules/k8sgitlabrunner"
  version = "0.1.0"

  kubeconfig    = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  project_slug  = var.project_slug
  project_name  = var.project_name
  gitlab_groups = [data.gitlab_group.deveco.group_id]
}

module "zammad" {
  source     = "./tools/zammad"
  kubeconfig = data.scaleway_k8s_cluster.prod.kubeconfig[0]
  hostname   = "zammad.${var.prod_base-domain}"

  project_slug = var.project_slug

  backup_bucket_name       = scaleway_object_bucket.backup_bucket_zammad.name
  backup_offset_minutes    = 45
  scaleway_project_id      = var.scaleway_cluster_production_project_id
  scaleway_organization_id = var.scaleway_organization_id
  scaleway_access_key      = var.scaleway_cluster_production_access_key
  scaleway_secret_key      = var.scaleway_cluster_production_secret_key
}
