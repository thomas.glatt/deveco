resource "scaleway_object_bucket" "backup_bucket_production" {
  provider = scaleway.scaleway_production
  name     = "${var.project_slug}-database-backups"
  acl      = "private"
  region   = "fr-par"
}

resource "scaleway_object_bucket" "backup_bucket_development" {
  provider = scaleway.scaleway_development
  name     = "${var.project_slug}-dev-database-backups"
  acl      = "private"
  region   = "fr-par"
}

resource "scaleway_object_bucket" "backup_bucket_zammad" {
  provider = scaleway.scaleway_production
  name     = "${var.project_slug}-zammad-backups"
  acl      = "private"
  region   = "fr-par"
}
