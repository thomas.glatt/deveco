module "repo-config_backend" {
  source  = "gitlab.com/vigigloo/tf-modules-gitlab/projectcommonconfiguration"
  version = "0.0.1"

  project_id  = data.gitlab_project.backend.id
  helm_values = file("${path.module}/config/backend.yml")
}
