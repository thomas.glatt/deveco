terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 3.12.0"
    }
    scaleway = {
      source  = "scaleway/scaleway"
      version = "~> 2.2.0"
    }
  }
  required_version = ">= 0.14"
}
