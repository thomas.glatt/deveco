locals {
  repositories = [data.gitlab_project.backend.id, data.gitlab_project.frontend.id]
}
module "reviews" {
  source                   = "./environment"
  gitlab_environment_scope = "*"
  kubeconfig               = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  base-domain              = var.dev_base-domain
  namespace                = "${var.project_slug}-reviews"
  repositories             = local.repositories
  project_slug             = var.project_slug
  project_name             = var.project_name

  namespace_quota_max_cpu    = 8
  namespace_quota_max_memory = "12Gi"

  scaleway_access_key = var.scaleway_cluster_development_access_key
  scaleway_secret_key = var.scaleway_cluster_development_secret_key

  backend_project_id = data.gitlab_project.backend.id

  backup_bucket_name = null
}

module "development" {
  source                   = "./environment"
  gitlab_environment_scope = "development"
  kubeconfig               = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  base-domain              = var.dev_base-domain
  namespace                = "${var.project_slug}-development"
  repositories             = local.repositories
  project_slug             = var.project_slug
  project_name             = var.project_name

  namespace_quota_max_cpu    = 8
  namespace_quota_max_memory = "12Gi"

  scaleway_access_key = var.scaleway_cluster_development_access_key
  scaleway_secret_key = var.scaleway_cluster_development_secret_key

  backend_project_id = data.gitlab_project.backend.id

  backup_bucket_name = scaleway_object_bucket.backup_bucket_development.name
}

module "production" {
  source                   = "./environment"
  gitlab_environment_scope = "production"
  kubeconfig               = data.scaleway_k8s_cluster.prod.kubeconfig[0]
  base-domain              = var.prod_base-domain
  namespace                = var.project_slug
  repositories             = local.repositories
  project_slug             = var.project_slug
  project_name             = var.project_name

  namespace_quota_max_cpu    = 8
  namespace_quota_max_memory = "12Gi"

  scaleway_access_key = var.scaleway_cluster_production_access_key
  scaleway_secret_key = var.scaleway_cluster_production_secret_key

  backend_project_id = data.gitlab_project.backend.id

  backup_bucket_name = scaleway_object_bucket.backup_bucket_production.name
}
